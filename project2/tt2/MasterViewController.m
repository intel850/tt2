//
//  MasterViewController.m
//  tt2
//
//  Created by ts on 12/02/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//b

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "DetailViewController2.h"

@implementation MasterViewController;

@synthesize detailViewController = _detailViewController;
@synthesize detailViewController2 = _detailViewController2;
@synthesize locationLabel;
@synthesize locationManager;
@synthesize geoCoder;
@synthesize tm;

		
- (void)dealloc
{
    self.detailViewController =nil;
    self.detailViewController2 =nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle   

- (void)viewDidLoad
{
    //make post_button
    //投稿ボタン
    UIButton *postbtn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    postbtn2.frame = CGRectMake(120, 280, 100, 30);
    [postbtn2 setTitle:@"メモ" forState:UIControlStateNormal];
    [postbtn2 addTarget:self action:@selector(post_btn_down2:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:postbtn2];
    
    //メモリチェック
    [self memCheck];
    
    //逆ジオここから    
    locationManager.delegate=self;
    [locationManager startUpdatingLocation];
    //5秒geo
    [self honGeo];
    tm =
        [NSTimer
         scheduledTimerWithTimeInterval:1.0f
         target:self
         selector:@selector(geoCodeLocation:)
         userInfo:nil
         repeats:YES
         ];
}

- (void)viewDidUnload
{
    [self setLocationLabel:nil];
    [self setLocationManager:nil];
    [self setGeoCoder:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

//座標取得
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	// 緯度経度取得に成功した場合
	CLLocationCoordinate2D coordinate = newLocation.coordinate;
	
    _latitude = coordinate.latitude;	// 緯度
	_longitude = coordinate.longitude;	// 経度
    
	// 試しにNSLogで出力
	NSLog(@"%f,%f", _latitude, _longitude);
}

//逆ジオ鈴木
- (void)honGeo{
    //ジオコーディングするこの記述法は本当は嫌いだお
    [self.geoCoder reverseGeocodeLocation: locationManager.location completionHandler: 
     ^(NSArray *placemarks, NSError *error) {
         
         //PlaceMarkをつかう　現在の位置を取得
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         //文字列に保存
         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         
         NSLog(@"I am currently at %@",locatedAt);
         
         //現在の位置を出力
         [locationLabel setText:locatedAt];
         
     }];
}

- (IBAction)geoCodeLocation:(NSTimer*)tm
{    
    [self honGeo];
}

//ナナビゲーションバーキャプション
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"TOP", @"Detail not View");
    }
    return self;
}

//メモリ情報の表示
-(void)memCheck{
    struct task_basic_info t_info;
	mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    
	if (task_info(current_task(), TASK_BASIC_INFO,
				  (task_info_t)&t_info, &t_info_count)!= KERN_SUCCESS) {
		NSLog(@"%s(): Error in task_info(): %s",
			  __FUNCTION__, strerror(errno));
	}
    
	u_int rss = t_info.resident_size;
	NSLog(@"RSS: %0.1f MB", rss/1024.0/1024.0);
}


//Enter押したらキーボードが隠れる。
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [memo_inputField resignFirstResponder];
    return NO;
}

//フォーカスがとれたらキーボードが隠れる。（複数レイヤーにわたってUIオブジェクトがある時チェインに注意）
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [memo_inputField resignFirstResponder];
}

//メモ投稿用画面遷移ボタン

-(void)post_btn_down2:(UIButton*)button
{    
    DetailViewController2 *detailViewController2 = [[DetailViewController2 alloc] initWithNibName:@"DetailViewController2" bundle:nil];
    [self.navigationController pushViewController:detailViewController2 animated:YES];
    [detailViewController2 release];
    
}


-(IBAction)search_convini_btn_down:(id)sender{
    //コンビニ
    //ここで日本語テキストを入れ、文字コード変換をするとうまくURLに入るようなのだが…うまく動かない。ので、この謎文字を入れる。
    NSString* addString = @"%E3%82%B3%E3%83%B3%E3%83%93%E3%83%8B";
    [self searchOpenGooglemap:addString];
    [self memCheck]; //メモリ状況チェック
}


- (IBAction) search_gasolineStand_btn_down:(id)sender
{
    //ガソリンスタンド
    NSString* addString = @"%E3%82%AC%E3%82%BD%E3%83%AA%E3%83%B3%E3%82%B9%E3%82%BF%E3%83%B3%E3%83%89";
    [self searchOpenGooglemap:addString];
}

- (IBAction) search_onsen_btn_down:(id)sender{
    //温泉
    NSString* addString = @"%E6%B8%A9%E6%B3%89";
    [self searchOpenGooglemap:addString];
}

- (IBAction) search_hotel_btn_down:(id)sender{
    //ホテル
    NSString* addString = @"%E3%83%9B%E3%83%86%E3%83%AB";
    [self searchOpenGooglemap:addString];    
}

- (IBAction) search_bike_shop_btn_down:(id)sender{
    //バイク屋
    NSString* addString = @"%E3%83%90%E3%82%A4%E3%82%AF%E5%B1%8B";
    [self searchOpenGooglemap:addString];
}

- (IBAction) search_campsite_btn_down:(id)sender{
    //キャンプ場
    NSString* addString = @"%E3%82%AD%E3%83%A3%E3%83%B3%E3%83%97%E5%A0%B4";
    [self searchOpenGooglemap:addString];
}

//各検索ボタン押下時にgoogleMAPを呼び出すメソッド
- (void)searchOpenGooglemap:(NSString*)btnWord{    
    
    NSString* searchUrl;
    searchUrl = [NSString stringWithFormat:@"http://maps.google.com/maps?z=15&q=%@&sll=%f,%f",btnWord,_latitude, _longitude];
    NSLog(@"searchUrl=%@" ,searchUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:searchUrl]];
    [self memCheck]; //メモリ状況チェック
    
}

//回転殺し
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;

}    
    
@end
