//
//  DetailViewController.m
//  tt2
//
//  Created by ts on 12/02/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"

#import "DetailViewController2.h"
//ここにDetailViewController.hのことも書かれている。
#import "MasterViewController.h"

@implementation DetailViewController
@synthesize re_value;
@synthesize newpost_Flg;

- (void)dealloc
{
    [super dealloc];
}

#pragma mark - Managing the detail item

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{

    NSLog(@"ooou %@", re_value);
    self.title = @"メモ";
    
    //右上編集ボタン
    UIBarButtonItem *btn = [[[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                             target:self action:@selector(memo_post_btn_down:)] autorelease];
    self.navigationItem.rightBarButtonItem = btn;
    
    //ud NMTAry
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *memoDataArrayBf = [[[NSMutableArray alloc] init]autorelease];
    memoDataArrayBf = [ud objectForKey:@"IMPUT_ARRAY"];
    
    
    //NSUserDefaultを直接書き換えすると発生するエラー回避の為、配列を移し替え
    NSMutableArray *memoDataArray = [NSMutableArray arrayWithArray:memoDataArrayBf];
    if([newpost_Flg isEqualToString:@"0"]){
        memo_inputView.text = [memoDataArray objectAtIndex:[re_value intValue]];
    }
    //容れ物として使われるmemoDataArrayBfが、unusedで怒られない為の呪文
    #pragma unused(memoDataArrayBf)
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

//回転殺し
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
    
}   
							
#pragma mark

//メモリ情報の表示
-(void)memCheck{
    struct task_basic_info t_info;
	mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    
	if (task_info(current_task(), TASK_BASIC_INFO,
				  (task_info_t)&t_info, &t_info_count)!= KERN_SUCCESS) {
		NSLog(@"%s(): Error in task_info(): %s",
			  __FUNCTION__, strerror(errno));
	}
    
	u_int rss = t_info.resident_size;
	NSLog(@"RSS: %0.1f MB", rss/1024.0/1024.0);
}

//フォーカスがとれたらキーボードが隠れる。（複数レイヤーにわたってUIオブジェクトがある時チェイン（レスポンダーが感知しない）に注意）
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//UITouch* touch = [touches anyObject];
	//CGPoint pt = [touch locationInView:self];
    [memo_inputField resignFirstResponder];
    NSLog(@"タッチ");
}


- (IBAction) memo_post_btn_down:(id)sender{
    
     //ボタン押下＆投稿（入力内容がある場合に実施）
     if(![memo_inputView.text isEqualToString:@""]){
         
         //updatelbl
         output_lbl.text = memo_inputView.text;
    
         //ud NMTAry
         NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
         NSMutableArray *memoDataArrayBf = [[[NSMutableArray alloc] init]autorelease];
         memoDataArrayBf = [ud objectForKey:@"IMPUT_ARRAY"];
         
         //NSUserDefaultを直接書き換えすると発生するエラー回避の為、配列を移し替え
         NSMutableArray *memoDataArray = [NSMutableArray arrayWithArray:memoDataArrayBf];
          NSLog(@"a22%@",memoDataArray);
         NSString *str = memo_inputView.text;
         
         if([newpost_Flg isEqualToString:@"1"]){
                [memoDataArray insertObject:str  atIndex:0];
            }else{
         
         //対応インデックス番号挿入
         [memoDataArray replaceObjectAtIndex:[re_value intValue] withObject:str];
            }
         [ud setObject:memoDataArray forKey:@"IMPUT_ARRAY"];
         
         //容れ物として使われるmemoDataArrayBfが、unusedで怒られない為の呪文
        #pragma unused(memoDataArrayBf)
         
         //新規フラグ解除
         newpost_Flg = @"0";
         
    //バック動作
    //[self.navigationController popViewControllerAnimated:YES];
     
     //memo_inputView.text = @"";
     NSLog(@"投稿実施");
     }else{
         //何も書かれていない時バック動作
         [self.navigationController popViewControllerAnimated:YES];
     
     }
    
     NSLog(@"投稿");
    [memo_inputView resignFirstResponder];
    [self memCheck]; //メモリ状況チェック
    
}

//削除ボタン
- (IBAction) delete_button_down:(id)sender{
     

    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"確認"
                          message:@"このメッセージを削除しますか？"
                          delegate:self
                          cancelButtonTitle:@"いいえ"
                          otherButtonTitles:@"はい", nil];
    [alert show];
    [alert release];

}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    [memo_inputView resignFirstResponder];
    return YES;
}

// アラートのボタンが押された時に呼ばれるデリゲート
-(void)alertView:(UIAlertView*)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            //１番目のボタンが押されたときの処理を記述する
            break;
        case 1:
            //２番目のボタンが押されたときの処理を記述する
             [self delete];
            break;
    }
    
}

- (void)delete{
            //ud NMTAry
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *memoDataArrayBf = [[[NSMutableArray alloc] init]autorelease];
    memoDataArrayBf = [ud objectForKey:@"IMPUT_ARRAY"];
    
    //NSUserDefaultを直接書き換えすると発生するエラー回避の為、配列を移し替え
    NSMutableArray *memoDataArray = [NSMutableArray arrayWithArray:memoDataArrayBf];
    
    if(![newpost_Flg isEqualToString:@"1"]){
        [memoDataArray removeObjectAtIndex:[re_value intValue]];
    }
    
    [ud setObject:memoDataArray forKey:@"IMPUT_ARRAY"];
    //[memoDataArrayBf release];
    //容れ物として使われるmemoDataArrayBfが、unusedで怒られない為の呪文
    #pragma unused(memoDataArrayBf)
    [self.navigationController popViewControllerAnimated:YES];

}


//メモ投稿用画面遷移ボタン元2
/*
-(void)post_btn_down2:(UIButton*)button
{
    DetailViewController2 *detailViewController2 = [[DetailViewController2 alloc] initWithNibName:@"DetailViewController2" bundle:nil];
    [self.navigationController pushViewController:detailViewController2 animated:YES];
    [detailViewController2 release];
}
*/
@end
