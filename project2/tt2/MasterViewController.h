//
//  MasterViewController.h
//  tt2 花子が居る状態２
//
//  Created by ts on 12/02/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#include <mach/mach.h>

@class DetailViewController;
@class DetailViewController2;

@interface MasterViewController :UIViewController<CLLocationManagerDelegate>
{
    /*
    // ロケーションマネージャー
	CLLocationManager* locationManager;
    */
	// 現在位置記録用
	CLLocationDegrees _longitude;
	CLLocationDegrees _latitude;
    
    //撮ってきた逆ジオここから
    //ただのロード画面
    UIActivityIndicatorView *indicator;
    UILabel *coordinateText;
    UILabel *addressText;
    CLLocationManager *locationManager;
    
    IBOutlet UIButton *search_convini_btn;
    IBOutlet UIButton *search_gasolineStand_btn;
    IBOutlet UIButton *search_onsen_btn;    //"Onsen" is correct. "Spa","Hot spring" are incorrect.
    IBOutlet UIButton *search_hotel_btn;
    IBOutlet UIButton *search_bike_shop;
    IBOutlet UIButton *search_campsite_btn;
    IBOutlet UIButton *post_btn;//ポストボタン
    IBOutlet UIButton *post_btn2;//ポストボタン
    
    IBOutlet UILabel *output_lbl;
    IBOutlet UILabel *now_Poslbl;
    IBOutlet UITextField *memo_inputField;    
    
    
}

@property (retain, nonatomic) DetailViewController *detailViewController;
@property (retain, nonatomic) DetailViewController2 *detailViewController2;

//逆ジオ鈴木
@property (nonatomic, retain) IBOutlet UILabel *locationLabel;
@property (nonatomic, retain) IBOutlet CLLocationManager *locationManager;
@property (nonatomic, retain) IBOutlet CLGeocoder *geoCoder;
@property (nonatomic, retain) NSTimer *tm;

- (IBAction) search_convini_btn_down:(id)sender;
- (IBAction) search_gasolineStand_btn_down:(id)sender;
- (IBAction) search_onsen_btn_down:(id)sender;
- (IBAction) search_hotel_btn_down:(id)sender;
- (IBAction) search_bike_shop_btn_down:(id)sender;
- (IBAction) search_campsite_btn_down:(id)sender;

- (void)searchOpenGooglemap:(NSString*)btnWord;
- (void)memCheck;//メモリチェック 

@end
