//
//  DetailViewController.h
//  tt2
//
//  Created by ts on 12/02/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <mach/mach.h>

@class MasterViewController;
@class DetailViewController2;

@interface DetailViewController : UIViewController{
    IBOutlet UILabel *output_lbl;
    IBOutlet UITextField *memo_inputField;
    IBOutlet UITextView *memo_inputView;
    IBOutlet UIBarItem *delete_button;

}

@property(retain) NSString *re_value;
@property(retain) NSString *newpost_Flg;
//@property(readonly) int seireki;


- (IBAction) delete_button_down:(id)sender;
- (void)delete;
//    - (IBAction)inputField;
- (void)memCheck;//メモリチェック

@end
